# Dido D library
Dido is D library that provides easy way to load functions and classes from shared libraries at runtime.

## Overview
Now Dido consists of three parts:

### Library
Library class allows to load shared libraries at runtime.

### ClassLoader
ClassLoader class allows to load classes from shared libraries and create their instances.

### Reflection
Reflector class allows to export structs from shared library. From application side reflected types can be handled by Reflection class.

## Supported platforms
Dido was tested on the following platforms:

* Debian jessie i686 with dmd and ldc2 (partially)
* Windows 7 with dmd (works partially)

## Documentation

Documentation can be generated with the next command:
`dmd -c dido/*.d -D candydoc/candy.ddoc candydoc/modules.ddoc`

## Licensing

Dido uses [Boost Software License](http://www.boost.org/LICENSE_1_0.txt "Boost Software License")

## TODO list:
* Make Dido crossplatform (should support FreeBSD, Linux, Mac OS X and Windows)
* Support all popular D compilers (dmd, gdc, ldc and, maybe, sdc).
* Provide tests for each supported platform and compiler.
* Make good documentation and Wiki
* Implement elf-parser.
* Make it possible to load functions from object files.
