/**
License:   <a href="http://www.boost.org/LICENSE_1_0.txt">Boost License 1.0</a>.
*/

module dido.classloader;

private import dido.library;

class ClassLoaderException : LibraryException
{
private:
    string _fileName;
public:
    this(string msg, string fileName)
    {
        super(msg);
        _fileName = fileName;
    }
    ///Name of class that failed to load
    @property fileName() const {
        return _fileName;
    }
}

class CreateInstanceException : ClassLoaderException
{
private:
    string _className;
public:
    this(string msg, string fileName, string className)
    {
        super(msg, fileName);
        _className = className;
    }
    @property className() const {
        return _className;
    }
}

///The ClassLoader loads classes from a library and creates its instances at runtime. I is a base class for loaded class.
class ClassLoader(I = Object) : Library
{
public:
    alias I BaseClass;
private:
    static if(is(I == interface))
    {
        enum bool Cpp = true;
        alias extern(C) I function(const(char)*) CreateFn;
        alias extern(C) const(char)** function() ClassNamesFn;
        alias extern(C) void function(I) DestroyFn;
        DestroyFn destroyFn;
    }
    else
    {
        enum bool Cpp = false;
        alias extern(C) I function(string) CreateFn;
        alias extern(C) string[] function() ClassNamesFn;
    }
    CreateFn createFn;
    ClassNamesFn classNamesFn;
    string[] _classNames;
protected:
    ///Sets all fields to default (null) state.
    override void clearAll()
    {
        super.clearAll();
        createFn = null;
        classNamesFn = null;
        _classNames = [];
        static if (Cpp)
        {
            destroyFn = null;
        }
    }
public:
    ///Constructs ClassLoader and loads all necessary functions in place.
    this(string fileName)
    {
        super(fileName);
    }
    this()
    {
        super();
    }
    ///Check if library is loaded properly.
    override @property bool isLoaded() const nothrow
    {
        bool ret = super.isLoaded && createFn && classNamesFn;
        static if(Cpp)
        {
            ret = ret && destroyFn;
        }
        return ret;
    }
    ///This property holds names of classes which instances can be created by ClassLoader.
    @property const(string)[] classNames() const nothrow
    {
        return _classNames;
    }
    /**Loads the library and all necessary functions to load class(es)
    Throws: $(B LoadException) if can't load the library or $(B ResolveException) if can't resolve required symbols.
    */
    override void load(string fileName, LoadHint hints = LoadHint.Default)
    {
        super.load(fileName, hints);
        createFn = this.resolve!CreateFn("dido_create");
        classNamesFn = this.resolve!ClassNamesFn("dido_classNames");
        static if (Cpp)
        {
            import std.stdio;
            for (const(char)** names = classNamesFn(); *names != null; names++)
            {
                _classNames ~= to!string(*names);
            }
            destroyFn = this.resolve!DestroyFn("dido_destroy");
        }
        else
        {
            _classNames = classNamesFn();
        }
    }
    /**Unloads the library and returns true if the library could be unloaded; otherwise returns false. 
    Note: 
        Don't unload the library when some instances of loaded classes are still alive. Usually you don't want to use this method because the moment when D objects get destroyed is not defined due to garbage collection. If you want to unload the library safely then use $(B scope) declaration to ensure that instances of loaded class(es) will be deleted in the end of the scope.
    Throws: $(B UnloadException) on failure.
    */
    override void unload()
    {
        super.unload();
    }
    /**Creates new instances of loaded classes.
    Params:
        className = name of class to create instance.
    Returns: Default constructed instance of specified class.
    Throws: $(B ClassLoaderException) if library is not loaded or $(B CreateInstanceException) if can't create instance of class with specified name.
    */
    I create(string className)
    {
        enforce(isLoaded, new ClassLoaderException(fileName~": library is not loaded to create instances", fileName));
        static if (Cpp)
        {
            I i = createFn(toStringz(className));
        }
        else
        {
            I i = createFn(className);
        }
        enforce(i !is null, new CreateInstanceException(fileName~": cannot create instance of class: "~className, fileName, className));
        return i;
    }
    /**Creates instance of the first class in class list. Useful when the library exports only one class.
    Returns: Default constructed instance of the first class in class list.
    Throws: $(B ClassLoaderException) if library is not loaded or $(B CreateInstanceException) if can't create instance of class.
    */
    I create()
    {
        enforce(classNames.length, new ClassLoaderException(fileName~": no classes loaded", fileName));
        return create(classNames[0]);
    }
    static if (Cpp)
    {
        ///Deletes instance of class. Allowed only for C++ classes.
        void destroy(I i)
        {
            enforce(isLoaded, new ClassLoaderException(fileName~": library is not loaded to destroy instance", fileName));
            destroyFn(i);
        }
    }
}

version(unittest)
{
    import unittests.classtest.baseclass;
    debug import std.stdio;

    extern(C++) interface Cpp
    {
        int getPayload() const;
        int doThing(int a, int b);
        void testThrow();
    }
}

unittest
{
    debug string expectedException = "Exception is caught as expected. Message: ";
    string libName = Library.cwd ~ Library.nativeName("dclass");
    auto plug = new ClassLoader!IDfoo(libName);
    assert(plug.isLoaded);
    foreach(string className; plug.classNames)
    {
        scope IDfoo inst = plug.create(className);
        int result = inst.dfoo(6,2);
        assert(result == inst.getPayload(), "Invalid result");
        version(Windows)
        {
            //DMD on Windows has problems with handling of exceptions from dll
        }
        else
        {
            try {
                inst.testThrow();
            }
            catch(Exception e)
            {
                debug writeln(expectedException, e.msg);
            }
        }
    }
    plug.unload();
    assert(!plug.isLoaded);
    
    libName = Library.cwd ~ Library.nativeName("cppclass");
    auto plug2 = new ClassLoader!Cpp(libName);
    assert(plug2.isLoaded);
    foreach(string className; plug2.classNames)
    {
        Cpp inst = plug2.create(className);
        int result = inst.doThing(6,2);
        assert(result == inst.getPayload(), "Invalid result");
        version(Windows)
        {
            //DMD on Windows has problems with handling of exceptions thrown from dll
        }
        else
        {
            try {
                inst.testThrow();
            }
            catch(Exception e)
            {
                debug writeln(expectedException, e.msg);
            }
        }
        plug2.destroy(inst);
    }
    plug2.unload();
    assert(!plug2.isLoaded);
}

