/**
License:   <a href="http://www.boost.org/LICENSE_1_0.txt">Boost License 1.0</a>.
*/

module dido.library;

package import dido.mangle;
package import std.string : toStringz;
package import std.exception : enforce;
package import std.conv : to;
package import std.traits;

version(Posix)
{
    version(linux)
    {
        import std.c.linux.linux;
    }
    else version(FreeBSD)
    {
        import core.sys.freebsd.dlfcn;
    }
    else
    {
        import core.sys.posix.dlfcn;
    }
    alias void* LibHandle;
}
else version(Windows)
{
    import std.c.windows.windows;
    import core.runtime;
    alias HMODULE LibHandle;
}
else
{
    static assert(false, "Unsupported platform");
}

///Adds C linkage to function type for the correct resolving
template ExternC(T) if (isSomeFunction!T)
{
    //Some hacks to add extern(C) to Type
    alias ReturnType!T R;
    alias ParameterTypeTuple!T Params;
    alias extern(C) R function (Params) ExternC;
}

///Adds C++ linkage to function type for the correct resolving
template ExternCpp(T) if (isSomeFunction!T)
{
    //Some hacks to add extern(C++) to Type
    alias ReturnType!T R;
    alias ParameterTypeTuple!T Params;
    alias extern(C++) R function (Params) ExternCpp;
}

///Base class for exceptions thrown from a Library
class LibraryException : Exception
{
    this(string msg, string file = __FILE__, size_t line = __LINE__) {
        super(msg, file, line);
    }
}
///Class for exceptions thrown from a Library.load
class LoadException : LibraryException
{
private:
    string _fileName;
public:
    this(string msg, string fileName) {
        super(msg);
        _fileName = fileName;
    }
///This property holds file name of library failed to load.
    @property string fileName() const {
        return _fileName;
    }
}
///Class for exceptions thrown from a Library.unload.
class UnloadException : LibraryException
{
private:
    string _fileName;
public:
    this(string msg, string fileName) {
        super(msg);
        _fileName = fileName;
    }
    ///This property holds file name of library failed to unload.
    @property string fileName() const {
        return _fileName;
    }
}
///Class for exceptions thrown from a Library.resolve and Library.resolveD.
class ResolveException : LibraryException
{
private:
    string _symbol;
    string _fileName;
public:
    this(string msg, string fileName, string symbol) {
        super(msg, file, line);
        _symbol = symbol;
        _fileName = fileName;
    }
    ///This property holds symbol failed to resolve.
    @property string symbol() const {
        return _symbol;
    }
    ///This property holds file name of library where it's failed to resolve symbol.
    @property string fileName() const {
        return _fileName;
    }
}

/** This enum describes the possible hints that can be used to change the way libraries are handled when they are loaded. 
The interpretation of the load hints is platform dependent, and if you use it you are probably making some assumptions on which platform you are compiling for, so use them only if you understand the consequences of them.
*/
enum LoadHint
{
    Default = 0, ///Implementation-specified default behaviour.
    ResolveAllSymbols = 1, ///Causes all symbols in a library to be resolved when it is loaded. This hint does nothing on Windows system.
    GlobalExport = 2, ///Causes all symbols in a library to be available for relocation processing of other modules. This hint has limited capabilities on Windows system.
}

private version (Windows)
{
    shared string _lastError;
    
    import std.container : RedBlackTree;
    shared RedBlackTree!LibHandle globalModules;
    
    string getModuleName(LibHandle handle)
    {
        char[MAX_PATH] moduleName;
        auto length = GetModuleFileName(handle, moduleName, MAX_PATH);
        return to!string(moduleName[0..length]);
    }
    
    synchronized void addGlobalModule(LibHandle handle)
    {
        if (handle != null)
            globalModules.stableInsert(handle);
    }
    synchronized void removeGlobalModule(LibHandle handle)
    {
        if (handle != null)
            globalModules.removeAny(handle);
    }
}

/**
 * Cross-platform wrapper for getting the last error message of library processing.
 * Returns: string that describes the last error of library processing. If no errors have occured since the last invocation of this function it returns $(B null).
 */
string lastLibraryError()
{
    version(Posix)
    {
        return to!string(dlerror());
    }
    else version(Windows)
    {
        string currentError = _lastError;
        _lastError = null;
        return currentError;
    }
}

/**
 * Cross-platform wrapper for loading library.
 * Returns: If the function succeds the return value is LibHandle object that can be passed to $(B unloadLibrary) and $(B getSymbolAddress) functions. Otherwise the return value is $(B null).
 */
LibHandle loadLibrary(string fileName, LoadHint loadHints = LoadHint.Default)
{
    version(Posix)
    {
        auto cstr = toStringz(fileName);
        int dlflags;
        if (loadHints & LoadHint.ResolveAllSymbols)
            dlflags |= RTLD_NOW;
        else
            dlflags |= RTLD_LAZY;
        if (loadHints & LoadHint.GlobalExport)
            dlflags |= RTLD_GLOBAL;
        else
            dlflags |= RTLD_LOCAL;
        return dlopen(cstr, dlflags);
    }
    else version(Windows)
    {
        _lastError = null;
        UINT lastErrorMode = SetErrorMode(SEM_FAILCRITICALERRORS);
        LibHandle hModule;
        
        if (fileName is null)
        {
            hModule = thisHandle;
        }
        else
        {
            auto cstr = toStringz(fileName);
            hModule = LoadLibraryEx(LPSTR(cstr), null, LOAD_WITH_ALTERED_SEARCH_PATH);
            if(!hModule)
            {
                _lastError = fileName ~ ": can't load library";
            }
            else if (loadHints & LoadHint.GlobalExport)
            {
                addGlobalModule(hModule);
            }
        }
        SetErrorMode(lastErrorMode);
        return hModule;
    }
}
/**
 * Cross-platform wrapper for unloading library.
 * Returns: $(B true) if library was unloaded successfully. Otherwise returns $(B false).
 */
bool unloadLibrary(LibHandle handle)
{
    version(Posix)
    {
        return dlclose(handle) == 0;
    }
    else version(Windows)
    {
        _lastError = null;
        bool result = FreeLibrary(handle) ? true : false;
        if (!result)
        {
            _lastError = getModuleName(handle) ~ ": can't unload library";
        }
        else
        {
            removeGlobalModule(handle);
        }
        return result;
    }
}

///Returns: handle for the main program.
@property LibHandle thisHandle()
{
    version(Posix)
    {
        return dlopen(null, RTLD_LAZY);
    }
    else version(Windows)
    {
        hModule = GetModuleHandle(null);
        if (!hModule) {
            _lastError = getModuleName(null) ~ ": can't load this module";
        }
        return hModule;
    }
}

/**
 * Cross-platform wrapper for getting address of symbol.
 * Returns: On success the return value is the address of the exported function or variable. On failure the return value is $(B null).
 */
void* getSymbolAddress(LibHandle handle, string symbol)
{
    version(Posix)
    {
        auto cstr = toStringz(symbol);
        return dlsym(handle, cstr);
    }
    else version(Windows)
    {
        _lastError = null;
        auto cstr = toStringz(symbol);
        FARPROC proc = GetProcAddress(handle, cstr);
        if (proc == null)
        {
            if (handle == thisHandle)
            {
                ///search symbol in global loaded modules
                foreach(LibHandle hndl; globalModules[])
                {
                    proc = GetProcAddress(hndl, cstr);
                    if (proc != null)
                        break;
                }
            }
        }
        if (proc == null)
            _lastError = getModuleName(handle) ~ ": can't resolve symbol "~symbol;
        return cast(void*)proc;
    }
}

/**
The Library can load shared libraries at runtime.
*/
class Library
{
private:
    LibHandle _handle;
    string _fileName;
    LoadHint _hints;

    @property bool isLoadedImpl() const nothrow
    {
        return _handle != null;
    }
    
protected:
    ///Sets all fields to default (null) state.
    void clearAll()
    {
        handle = null;
        fileName = null;
        loadHints = LoadHint.Default;
    }

public:
    /**
    Constructs Library object and load it in place
    Params:
        fileName = file name of shared library
        loadHints = hints for library loading
    Throws: 
    $(B LoadException) on failure
    */
    this(string fileName, LoadHint loadHints = LoadHint.Default)
    {
        this.load(fileName, loadHints);
    }
    /**
     * Constructs Library object from platform-dependent library handle. 
     * Note that obtained instance will have empty fileName and default loadHints fields. Using instance constructed in this way may be unsafe. To work with platform-dependent library handles better use wrapper functions.
     */
    this(LibHandle hndl)
    {
        handle = hndl;
    }
    /**
     * Default constructor.
     */
    this() {}
    ///This property holds platform-dependent shared library handle or $(B null) if library is not loaded.
    @property LibHandle handle() nothrow {
        return _handle;
    }
    protected @property LibHandle handle(LibHandle hndl) {
        return _handle = hndl;
    }
    ///This property holds the file name of the library.
    @property string fileName() const nothrow {
        return _fileName;
    }
    protected @property string fileName(string name) {
        return _fileName = name;
    }
    ///This property holds the hints used for library loading.
    @property LoadHint loadHints() const nothrow {
        return _hints;
    }
    protected @property LoadHint loadHints(LoadHint hints) {
        return _hints = hints;
    }

    ///Check if library is loaded.
    @property bool isLoaded() const nothrow {
        return isLoadedImpl;
    }

    /**
    Loads the library. If another library was already loaded for this instance, the new one is loaded. Note that this function does not unload the last library.
    Params:
        fileName = file name of shared library
        loadHints = hints for library loading
    Throws: $(B LoadException) on failure.
    */
    void load(string fileName, LoadHint loadHints = LoadHint.Default)
    {
        this.handle = null;
        this.fileName = fileName;
        this.loadHints = loadHints;
        this.handle = loadLibrary(fileName, loadHints);
        enforce(isLoadedImpl, new LoadException(lastLibraryError(), fileName));
    }

    /**
    Unloads the library.
    Throws: $(B UnloadException) on failure.
    */
    void unload()
    {
        scope(success) clearAll();
        enforce(isLoadedImpl, new UnloadException(fileName~": library is not loaded to be unload", fileName));
        bool result = unloadLibrary(handle);
        enforce(result, new UnloadException(lastLibraryError(), fileName));
    }

    /**
    Returns: The address of the exported symbol. The library needs to be loaded.
    Throws: $(B ResolveException) on failure.
    */
    void* resolve()(string symbol)
    {
        enforce(isLoadedImpl, new ResolveException(fileName~": library is not loaded to resolve symbol: "~symbol, fileName, symbol));
        void* ret = getSymbolAddress(handle, symbol);
        enforce(ret != null, new ResolveException(lastLibraryError(), fileName, symbol));
        return ret;
    }
    ///Casts address of exported symbol to specified type.
    T* resolve(T:T*)(string symbol) {
        return cast(T*)resolve(symbol);
    }
    ///ditto
    T* resolve(T)(string symbol) {
        return resolve!(T*)(symbol);
    }
    ///Returns: the address of identifier, specified by S. Casts address to typeof(S). 
    auto resolve(alias S)() {
        return resolve!(typeof(S)*)(__traits(identifier, S));
    }

    ///Same as $(B resolve) variants, but tries to use D-mangling. To load symbol you should provide its type and the whole name, including module path, for example "qwerty.foo.bar".
    T* resolveD(T:T*)(string symbol) {
        return resolve!(T*)(exportedDName!T(symbol));
    }
    ///ditto
    T* resolveD(T)(string symbol) {
        return resolve!(T*)(exportedDName!T(symbol));
    }
    ///ditto
    auto resolveD(alias S)() {
        version(Windows) {
            return resolve!(typeof(S))(S.mangleof[1..$]);
        }
        else {
            return resolve!(typeof(S))(S.mangleof);
        }
    }

    ///Returns: $(B true) if symbol can be resolved from library. Otherwise returns $(B false).
    bool canResolve(string symbol)
    {
        if (!isLoaded)
            return false;
        auto hndl = cast(LibHandle)_handle;
        void* ret = getSymbolAddress(hndl, symbol);
        return ret != null;
    }

    ///Returns: $(B true) if D-symbol can be resolved from library. Otherwise returns $(B false).
    bool canResolveD(T)(string symbol)
    {
        return canResolve(exportedDName!T(symbol));
    }
    /**
     * Returns: platform-dependent current work directory string including final directory separator
     * Throws: FileException on error. 
    */
    @property static string cwd()
    {
        import std.file : getcwd, dirSeparator;
        return getcwd() ~ dirSeparator;
    }
    ///Platform-dependent shared library suffix (file extension). For example, .so for GNU/Linux or .dll for Windows.
    @property static string suffix()
    {
        version(OSX)
        {
            return ".dyn";
        }
        else version(Posix)
        {
            return ".so";
        }
        else version(Windows)
        {
            return ".dll";
        }
    }
    ///Platform-dependent shared library prefix. For example, 'lib' for GNU/Linux or empty string for Windows.
    @property static string prefix()
    {
        version(Posix)
        {
            return "lib";
        }
        else version(Windows)
        {
            return string.init;
        }
    }
    ///Returns: name prepended by Library.prefix and appended by Library.suffix
    static string nativeName(string name)
    {
        return Library.prefix ~ name ~ Library.suffix;
    }
}

version(Posix)
{
    /**Undocumented hack to get shared library's address in memory by its handle. Only for GNU/Linux (and possibly other Posix) systems.
     * Returns:
        * null if handle is null. Otherwise returns supposed library's address in memory. Note that $(B handle) must be valid library handle obtained from $(B loadLibrary) or platform-dependent function for loading library.
     */
    void* libraryAddress(void* handle)
    {
        if(!handle)
            return null;
        return cast(void*)*(cast(const(size_t)*)handle);
    }
    void* libraryAddress(Library lib)
    {
        return libraryAddress(lib.handle);
    }
}

version(unittest)
{
    int dummy(int, int)
    {
        return 0;
    }

    debug import std.stdio;
    import unittests.libtest.funcs;
}

unittest
{
    string libName = Library.cwd ~ Library.nativeName("foo");
    auto lib = new Library(libName);
    assert(lib.isLoaded);

    alias int function(int, int) symbolType;

    assert(lib.canResolve("cfoo"));
    auto cfoo1 = cast(ExternC!symbolType)lib.resolve("cfoo");
    assert(cfoo1(3,4) == 7);

    auto cfoo2 = lib.resolve!(ExternC!symbolType)("cfoo");
    assert(cfoo2(3,4) == 7);

    auto cfoo3 = lib.resolve!(ExternC!(typeof(dummy)))("cfoo");
    assert(cfoo3(3,4) == 7);

    assert(lib.canResolveD!(symbolType)("unittests.libtest.funcs.dfoo"));
    auto dfoo1 = lib.resolveD!(symbolType)("unittests.libtest.funcs.dfoo");
    assert(dfoo1(3,4) == 7);

    auto dfoo2 = lib.resolveD!(typeof(dummy))("unittests.libtest.funcs.dfoo");
    assert(dfoo2(3,4) == 7);

    auto dfoo3 = lib.resolveD!(unittests.libtest.funcs.dfoo)();
    assert(dfoo3(3,4) == 7);
    
    debug string expectedException = "Exception is caught as expected. Message: ";

    version(Windows)
    {
        //DMD on Windows has problems with handling of exceptions thrown from dll
    }
    else 
    {
        auto testThrow = lib.resolveD!(void function())("unittests.libtest.funcs.testThrow");
        try {
            testThrow();
        }
        catch(Exception e)
        {
            debug writeln(expectedException, e.msg);
        }
    }

    try {
        lib.resolve("unexisting symbol");
    }
    catch(ResolveException e)
    {
        debug writeln(expectedException, e.msg);
    }

    lib.unload();
    assert(!lib.isLoaded);

    try {
        lib.load("unexisting library");
    }
    catch(LoadException e) 
    {
        debug writeln(expectedException, e.msg);
        try {
            lib.resolve("something");
        }
        catch(ResolveException e)
        {
            debug writeln(expectedException, e.msg);
        }
        try {
            lib.unload();
        }
        catch(UnloadException e)
        {
            debug writeln(expectedException, e.msg);
        }
    }
}

