/**
License:   <a href="http://www.boost.org/LICENSE_1_0.txt">Boost License 1.0</a>.
*/

module dido.classexport;

private template CreateByCase(T...)
{
    static if (T.length)
        immutable CreateByCase = "case \"" ~ T[0].stringof ~ "\":" ~ "return new " ~ T[0].stringof ~ ";\n" ~ CreateByCase!(T[1..$]);
    else
        immutable CreateByCase = "";
}

private template TypesToStrings(T...)
{
    static if (T.length)
        immutable TypesToStrings = "\""~T[0].stringof~"\","~TypesToStrings!(T[1..$]);
    else
        immutable TypesToStrings = "";
}

private template isDerivedFrom(D, B) if ((is(B == interface) || is(B == class)) && is(D == class))
{
    enum bool isDerivedFrom = is(D:B);
}

private template isBaseForAll(I, T...)
{
    static if (T.length)
        enum bool isBaseForAll = isDerivedFrom!(T[0], I) && isBaseForAll!(I, T[1..$]);
    else
        enum bool isBaseForAll = true;
}

/**Exports classes allowing to load them at runtime with ClassLoader object. $(BR)
    $(B I) is the base class for all exported classes $(BR)
    $(B T...) are exported classes
*/
template ExportClasses(I, T...) if (isBaseForAll!(I, T) && T.length)
{
    immutable ExportClasses = "export extern(C) "~I.stringof~" dido_create(string className){\nswitch(className){\n"~CreateByCase!(T)~"default: return null;}\n}\nexport extern(C) string[] dido_classNames(){return ["~TypesToStrings!(T)~"];}";
}
