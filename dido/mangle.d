/**
License:   <a href="http://www.boost.org/LICENSE_1_0.txt">Boost License 1.0</a>.
*/

module dido.mangle;

private import std.conv : to;
private import std.algorithm : splitter, reduce;
private import std.range : retro;

/**Mangles D-symbol. T is type of symbol. You need to provide the whole name of symbol, including the module path, for example "qwerty.foo.bar"
Returns: mangled name
*/
string mangleD(T)(string symbol)
{
    string str = reduce!(function(string a, string b){return a ~ to!string(b.length) ~ b;})(string.init, splitter(symbol, '.'));
    return "_D" ~ str ~ T.mangleof;
}
///ditto
string mangleD(T:T*)(string symbol)
{
    return mangleD!T(symbol);
}

///Platform dependent name of exported D symbol. It may differ from mangled one.
string exportedDName(T)(string symbol)
{
    version(Windows) {
        return mangleD!T(symbol)[1..$];
    }
    else {
        return mangleD!T(symbol);
    }
}

version(unittest)
{
    void dbar(int, float*)
    {

    }
}


unittest
{
    assert(dbar.mangleof == mangleD!(typeof(dbar))("dido.mangle.dbar"));
    assert(dbar.mangleof == mangleD!(void function(int, float*))("dido.mangle.dbar"));
}


