/**
License:   <a href="http://www.boost.org/LICENSE_1_0.txt">Boost License 1.0</a>.
*/

module dido.reflection;
import std.traits;
import std.exception;

private template MemberPointer(T, string member, FT = void)
if ((is(T == struct) || is(T == class)) && (isSomeFunction!(FT) || is(FT == void)))
{
    static assert(__traits(hasMember, T, member), T.stringof ~ " has no member " ~ member);
    immutable func = T.stringof~"."~member;

    static if (is(T == struct))
        alias T* Self;
    else
        alias T Self;

    alias typeof(mixin(func)) MemberType;

    static if(is(MemberType == function))
    {
        static if (is(FT == void))
        {
            static assert(__traits(getOverloads, T, member).length == 1, "ambiguity: member function '"~member~"' of class/struct '"~T.stringof~"' has overloads. You should specify function type in the third template parameter");
            alias ParameterTypeTuple!(mixin(func)) Args;
            
        }
        else
        {
            alias ParameterTypeTuple!(FT) Args;
        }
        auto MemberPointer =
            function(Instance inst, Args args)
            {
                auto self = cast(Self)inst.pointer;
                mixin("return self."~member~"(args);");
            };
    }
    else
    {
        static assert(is(FT == void), "member '"~member~"' is not function so it can't have argument list");
        auto MemberPointer = 
            function Reference!MemberType (Instance inst)
            {
                auto self = cast(Self)inst.pointer;
                return Reference!MemberType(mixin("&self."~member));
            };
    }       
}

private template Constructor(T, Args...)
if (is(T == struct) || is(T == class))
{
    auto Constructor = function(Args args)
    {
        return new Instance(new T(args));
    };
}

private template Destructor(T)
if (is(T == struct) || is(T == class))
{
    auto Destructor = function(T t)
    {
        destroy(t);
    };
}

private template FunctionPointerType(R, Args...)
{
    alias R function(Args) FunctionPointerType;
}

///Class to handle instances of reflected types. Used in reflected functions as first argument.
class Instance
{
private:
    void* _ptr;
package:
    this(T)(T t)
    {
        _ptr = cast(void*)t;
    }
    @property void* pointer()
    {
        return _ptr;
    }
}

///This class allows retrieving functions from reflection on client side.
abstract class Reflection
{
protected:
    void*[string] _members;
    void*[string] _constructors;
    void function(Instance) _destructor;
    TypeInfo _typeInfo;
    bool _isClass;
    string _typeName;
public:
    /**Tests whether reflected type is class or not.
    Returns: $(B true) if reflected type is class. $(B false) if reflected type is struct.
    */
    @property bool isClass() const nothrow
    {
        return _isClass;
    }
    /**
    Returns: reflection (function pointer) of member function.
    Throws: $(B RangeError) on failure.
    */
    auto getMethod(R, Args...)(string name) const
    {
        return cast(FunctionPointerType!(R, Instance, Args))_members[name];
    }
    /**
    Returns: reflection (function pointer) of member data.
    Throws: $(B RangeError) on failure.
    */
    auto getData(R)(string name) const
    {
        return cast(FunctionPointerType!(Reference!R, Instance))_members[name];
    }
    /**
    Returns: reflection (function pointer) of constructor.
    Throws: $(B RangeError) on failure.
    */
    auto getConstructor(Args...)(string name) const
    {
        return cast(FunctionPointerType!(Instance, Args))_constructors[name];
    }
    ///Returns: true if Reflection has method with given name
    bool haveMethod(string name) const
    {
        auto ret = name in _members;
        return ret != null;
    }
    ///Returns: true if Reflection has constructor with given name
    bool haveConstructor(string name) const
    {
        auto ret = name in _constructors;
        return ret != null;
    }
    ///Returns: name of reflected type.
    @property string typeName() const nothrow
    {
        return _typeName;
    }
    ///Returns: $(B TypeInfo) related to reflected type.
    @property inout(TypeInfo) typeInfo() inout nothrow
    {
        return _typeInfo;
    }
    /**
    Returns: $(B TypeInfo_Class) related to reflected type.
    Throws: $(B Exception) if reflected type is not class.
    */
    @property inout(TypeInfo_Class) classInfo() inout
    {
        enforce(isClass, new Exception(_typeName~" is not class so it has no classinfo"));
        return cast(typeof(return))_typeInfo;
    }
}

///Class used from library side to export type.
class Reflector(T) : Reflection
{
    this()
    {
        static if (is(T == struct))
        {
            _typeInfo = typeid(T);
            _isClass = false;
        }
        else static if (is(T == class))
        {
            _typeInfo = T.classinfo;
            _isClass = true;
        }
        _typeName = T.stringof;
    }
    /**
    Adds member to reflection. Specify FT template parameter with type of function if you need to reflect overloaded member.
    Params:
        pseudonym = alias for member in reflection. Omit this argument to force using the first template parameter as alias.
    */
    Reflector member(string name, FT = void)(string pseudonym = null)
    {
        auto m = cast(void*)MemberPointer!(T, name, FT);
        _members[pseudonym ? pseudonym : name] = m;
        return this;
    }
    ///Adds constructor to reflection. 
    Reflector constructor(Args...)(string pseudonym)
    {
        auto c = cast(void*)Constructor!(T, Args);
        _constructors[pseudonym] = c;
        return this;
    }
}

/**This struct represents wrapper around pointer and makes it look like $(B ref) type.
*/
struct Reference(T)
{
private:
    T* ptr;
public:
    this(T* t)
    {
        ptr = t;
    }
    @property ref T get()
    {
        return *ptr;
    }
    alias get this;
}

version(unittest)
{
    import std.conv;
    struct Struct
    {
        int data;
        int get() const {return data;}
        void set(int d) {data = d;};
        int overloaded_func(int d) const {return data*d;}
        string overloaded_func(string s) const {return to!string(data)~s;}
    }
}

unittest
{
    auto reflector = new Reflector!Struct;
    reflector
        .constructor("ctor")
        .member!("data")
        .member!("get")
        .member!("set")
        .member!("overloaded_func", int function(int))
        .member!("overloaded_func", string function(string))("overloaded_func2");
    
    Reflection br = reflector;
    auto ctor = br.getConstructor("ctor");
    auto data = br.getData!(int)("data");
    auto get = br.getMethod!(int)("get");
    auto set = br.getMethod!(void, int)("set");
    auto func = br.getMethod!(int, int)("overloaded_func");
    auto func2 = br.getMethod!(string, string)("overloaded_func2"); 
    auto inst = ctor();
    data(inst) = 5;
    assert(get(inst) == 5);
    set(inst, 7);
    assert(get(inst) == 7);
    data(inst)++;
    assert(data(inst) == 8);
    assert(func(inst, 3) == 24);
    assert(func2(inst, "some") == "8some");
}
