/**
License:   <a href="http://www.boost.org/LICENSE_1_0.txt">Boost License 1.0</a>.
*/
module dido.elf;

private import core.stdc.stdint;
private import std.exception;
private import std.conv : to;
private import std.c.string : strlen;
private import std.stdio;

immutable ubyte ElfMagic[4] = [0x7f, 'E', 'L', 'F'];

struct ElfHeader32
{
    ubyte magic[4] = ElfMagic;
    ElfClass eclass;
    ElfData data;
    ubyte _version;
    ubyte osabi;
    ubyte abiversion;
    ubyte padding[7];

    ElfType type;
    ElfMachine machine;
    ElfVersion eversion;
    uint32_t entry;
    uint32_t phoff;
    uint32_t shoff;
    uint32_t flags;
    uint16_t ehsize;
    uint16_t phentsize;
    uint16_t phnum;
    uint16_t shentsize;
    uint16_t shnum;
    uint16_t shstrndx;
}

struct ElfHeader64
{
    ubyte magic[4] = ElfMagic;
    ElfClass eclass;
    ElfData data;
    ubyte _version;
    ubyte osabi;
    ubyte abiversion;
    ubyte padding[7];

    ElfType type;
    ElfMachine machine;
    ElfVersion eversion;
    uint64_t entry;
    uint64_t phoff;
    uint64_t shoff;
    uint32_t flags;
    uint16_t ehsize;
    uint16_t phentsize;
    uint16_t phnum;
    uint16_t shentsize;
    uint16_t shnum;
    uint16_t shstrndx;
}

enum ElfClass : ubyte
{
    NONE = 0,  ///Invalid class
    _32 =   1, ///32-bit objects
    _64 =   2, ///64-bit objects
}

enum ElfData : ubyte
{
    NONE = 0,
    _2LSB = 1,
    _2MSB = 2,
}

enum ElfIdent : ubyte
{
    CLASS = 4,
    DATA = 5,
    VERSION = 6,
    ABIVERSION = 8,
    PAD = 9,
    IDENT = 16,
}

///The object file type
enum ElfType : uint16_t
{
    NONE = 0, ///No filetype
    REL = 1,  ///Relocatable file
    EXEC = 2, ///Executable file
    DYN = 3,  ///Shared object file
    CORE = 4, ///Core file
    LOPROC = 0xff00, ///Processor-specific
    HIPROC = 0xffff, //////Processor-specific
}

///Architecture
enum ElfMachine : uint16_t
{
    NONE =  0, ///No machine
    M32 =   1, ///AT&T WE 32100
    SPARC = 2, ///SPARC
    I386 =  3, ///Intel 80386
    M68K =  4, ///Motorola 68000
    M88K =  5, ///Motorola 88000
    I860 =  7, ///Intel 80860
    MIPS =  8, ///MIPS RS3000
}

///Elf version number
enum ElfVersion : uint32_t
{
    NONE = 0,
    CURRENT = 1,
}

struct SectionHeader32
{
    uint32_t name;
    SectionType type;
    uint32_t flags;
    uint32_t addr;
    uint32_t offset;
    uint32_t size;
    uint32_t link;
    uint32_t info;
    uint32_t addralign;
    uint32_t entsize;
}

struct SectionHeader64
{
    uint32_t name;
    SectionType type;
    uint64_t flags;
    uint64_t addr;
    uint64_t offset;
    uint64_t size;
    uint32_t link;
    uint32_t info;
    uint64_t addralign;
    uint64_t entsize;
}

enum SectionIndex
{
    UNDEF =       0,
    LORESERVE =   0xff00,
    LOPROC =      0xff00,
    HIPROC =      0xff1f,
    LOOS =        0xff20,
    ABS =         0xfff1,
    COMMON =      0xfff2,
    HIRESERVE =   0xffff,
}

enum SectionType : uint32_t
{
    NULL =          0,
    PROGBITS =      1,
    SYMTAB =        2,
    STRTAB =        3,
    RELA =          4,
    HASH =          5,
    DYNAMIC =       6,
    NOTE =          7,
    NOBITS =        8,
    REL =           9,
    SHLIB =         10,
    DYNSYM =        11,
    LOPROC =        0x70000000,
    HIPROC =        0x7fffffff,
    LOUSER =        0x80000000,
    HIUSER =        0x8fffffff,
}

struct ProgramHeader32
{
    uint32_t type;
    uint32_t offset;
    uint32_t vaddr;
    uint32_t paddr;
    uint32_t filesz;
    uint32_t memsz;
    uint32_t flags;
    uint32_t palign;
}

struct ProgramHeader64
{
    uint32_t type;
    uint32_t flags;
    uint64_t offset;
    uint64_t vaddr;
    uint64_t paddr;
    uint64_t filesz;
    uint64_t memsz;
    uint64_t palign;
}

struct ElfSymbol32
{
    uint32_t name;
    uint32_t value;
    uint32_t size;
    ubyte info;
    ubyte other;
    uint16_t shndx;
}

struct ElfSymbol64
{
    uint32_t name;
    ubyte info;
    ubyte other;
    uint16_t shndx;
    uint64_t value;
    uint64_t size;
}

enum SymbolBinding
{
    LOCAL =       0,
    GLOBAL =      1,
    WEAK =        2,
    LOPROC =      13,
    HIPROC =      15,
}

enum SymbolType
{
    NOTYPE =      0,
    OBJECT =      1,
    FUNC =        2,
    SECTION =     3,
    FILE =        4,
    LOPROC =      13,
    HIPROC =      15,
}

struct ElfRel32
{
    uint32_t r_offset;
    uint32_t r_info;
}

struct ElfRel64
{
    uint64_t r_offset;
    uint64_t r_info;
}

struct ElfRela32
{
    uint32_t r_offset;
    uint32_t r_info;
    int32_t  r_addend;
}

struct ElfRela64
{
    uint64_t r_offset;
    uint64_t r_info;
    int64_t  r_addend;
}

auto symbolBind(T)(T val) pure nothrow { return cast(ubyte)val >> 4; }
auto symbolType(T)(T val) pure nothrow { return val & 0xf; }
auto symbolInfo(B, T)(B bind, T type) pure nothrow { return (bind << 4) + (type & 0xf); }


auto ELF32_R_SYM(V)(V val) { return val >> 8; }
auto ELF32_R_TYPE(V)(V val) { return val & 0xff; }
auto ELF32_R_INFO(S, T)(S sym, T type) { return (sym << 8) + (type & 0xff); }

auto ELF64_R_SYM(I)(I i) { return i >> 32; }
auto ELF64_R_TYPE(I)(I i) { return i & 0xffffffff; }
auto ELF64_R_INFO(S, T)(S sym, T type) { return (sym << 32) + (type); }

string getBindName(ubyte info) pure nothrow
{
    switch(symbolBind(info))
    {
        case SymbolBinding.LOCAL:   return("Local");
        case SymbolBinding.GLOBAL:  return("Global");
        case SymbolBinding.WEAK:    return("Weak");
        default: return "Unknown";
    }
}

string getTypeName(ubyte info) pure nothrow
{
    switch(symbolType(info))
    {
        case SymbolType.NOTYPE:  return("None");
        case SymbolType.OBJECT:  return("Object");
        case SymbolType.FUNC:    return("Function");
        case SymbolType.SECTION: return("Section");
        case SymbolType.FILE:    return("File");
        default: return "Unknown";
    }
    
}

private inout(T)[] fromBytes(T, N, O)(inout(ubyte)[] bytes, N num, O offset = 0) 
if(__traits(isIntegral, N) && __traits(isIntegral, O))
{
    auto data = bytes[cast(size_t)offset..$];
    enforce(data.length >= num*T.sizeof, new Exception("Size mismatch"));
    return cast(typeof(return))data[0..cast(size_t)num*T.sizeof];
}

private T[] fromFile(T, N, O)(File file, N num, O offset)
{
    if (num)
    {
        file.seek(cast(size_t)offset);
        auto ret = file.rawRead(new T[cast(size_t)num]);
        enforce(ret.length == num, new Exception("Size mismatch"));
        return ret;
    }
    return [];
}

struct Elf32
{
    ElfHeader32 header;
    SectionHeader32[] sections;
    ProgramHeader32[] programs;
    ElfRela32[] relas;
}

struct Elf64
{
    ElfHeader64 header;
    SectionHeader64[] sections;
    ProgramHeader64[] programs;
    ElfRela64[] relas;
}

class ElfException : Exception
{
private:
    string _fileName;
public:
    this(string msg, string fileName)
    {
        super(msg);
    }
    @property string fileName() const {
        return fileName;
    }
}

class ElfObject
{
private:
    File file;
    Elf32* elf32;
    Elf64* elf64;

    this(Elf32* e)
    {
        elf32 = e;
    }
    this(Elf64* e)
    {
        elf64 = e;
    }
public:
    ElfClass elfClass() pure nothrow const
    {
        if (elf32)
            return ElfClass._32;
        else if (elf64)
            return ElfClass._64;
        else
            return ElfClass.NONE;
    }
    Elf32* getElf32() {
        enforce(elf32, new Exception("Elf Object is not 32-bit"));
        return elf32;
    }
    Elf64* getElf64() {
        enforce(elf64, new Exception("Elf Object is not 64-bit"));
        return elf64;
    }
    
    version(X86) {
        Elf32* getElf() { return getElf32(); }
    }
    else version(X86_64) {
        Elf64* getElf() { return getElf64(); }
    }
}

ElfObject parseElf(string fileName)
{
    auto file = File(fileName, "rb");
    ubyte[ElfIdent.IDENT] fixIdent;
    auto ident = file.rawRead(fixIdent[]);
    enforce(ident[0..4] == ElfMagic);

    auto edata = ident[ElfIdent.DATA];
    switch(edata)
    {
        case ElfData._2LSB:
            debug writeln("ElfData 2LSB");
            break;
        case ElfData._2MSB:
            debug writeln("ElfData 2MSB");
            throw new ElfException("2MSB currently not supported", fileName);
        default:
            throw new ElfException("Invalid data encoding", fileName);
    }

    auto eclass = ident[ElfIdent.CLASS];
    switch(eclass)
    {
        case ElfClass._32:
            debug writeln("ElfClass 32");
            return parseImpl!(ElfClass._32)(file);
        case ElfClass._64:
            debug writeln("ElfClass 64");
            return parseImpl!(ElfClass._64)(file);
        default:
            throw new ElfException("Invalid ElfClass", fileName);
    }
}

private ElfObject parseImpl(ElfClass ec)(File file)
{
    static if (ec == ElfClass._32)
    {
        alias ElfHeader32 ElfHeader;
        alias SectionHeader32 SectionHeader;
        alias ProgramHeader32 ProgramHeader;
        alias ElfSymbol32 ElfSymbol;
        alias ElfRel32 ElfRel;
        alias Elf32 Elf;
    }
    else static if (ec == ElfClass._64)
    {
        alias ElfHeader64 ElfHeader;
        alias SectionHeader64 SectionHeader;
        alias ProgramHeader64 ProgramHeader;
        alias ElfSymbol64 ElfSymbol;
        alias ElfRel64 ElfRel;
        alias Elf64 Elf;
    }
    else
    {
        static assert(false);
    }

    file.seek(0);

    ElfHeader[1] eharr;
    auto ehslice = file.rawRead(eharr[]);
    enforce(ehslice.length == 1, new ElfException("Size mismatch", file.name));

    auto eheader = ehslice[0];
    
    switch(eheader.eversion)
    {
        case ElfVersion.CURRENT:
            break;
        default:
            throw new ElfException("Invalid version number", file.name);
    }

    switch(eheader.type)
    {
        case ElfType.NONE:
            debug writeln("No file type");
            break;
        case ElfType.REL:
            debug writeln("Relocatable file");
            break;
        case ElfType.EXEC:
            debug writeln("Executable file");
            break;
        case ElfType.DYN:
            debug writeln("Shared object file");
            break;
        case ElfType.CORE:
            debug writeln("Core file");
            break;
        default:
            debug writeln("Unknown file type");
            break;
    }

    auto sections = fromFile!SectionHeader(file, eheader.shnum, eheader.shoff);
    auto programs = fromFile!ProgramHeader(file, eheader.phnum, eheader.phoff);

    auto nameSection = sections[eheader.shstrndx];
    auto sectionNames = fromFile!char(file, cast(size_t)nameSection.size, nameSection.offset);

    foreach(section; sections)
    {
        debug writeln("Section: ", to!string(sectionNames.ptr + section.name));
        switch(section.type)
        {
            case SectionType.SYMTAB:
            case SectionType.DYNSYM:
                auto stringSection = sections[section.link];
                auto stringTable = fromFile!char(file, stringSection.size, stringSection.offset);
                    
                auto symbols = fromFile!ElfSymbol(file, section.size/ElfSymbol.sizeof, section.offset);

                foreach(sym; symbols)
                {
                    debug if (sym.name)
                    {
                        auto str = to!string(stringTable.ptr + sym.name);
                        writefln("%s\t%s\t%s", str, getBindName(sym.info), getTypeName(sym.info));
                    }
                }
            break;

            case SectionType.REL: 
                auto rels = fromFile!ElfRel(file, section.size/ElfRel.sizeof, section.offset);
            break;

            default:
            break;
        }
    }

    return null;
}
