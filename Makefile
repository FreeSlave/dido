PROJECT_NAME=dido

DCFLAGS=
export SOURCE_DIR=$(PROJECT_NAME)
export BUILD_DIR=build
export INCLUDE_DIR=include
export LIB_DIR=lib
export DDOC_DIR=ddoc

SOURCE_PATH=$(SOURCE_DIR)$(PATH_SEP)
BUILD_PATH=$(BUILD_DIR)$(PATH_SEP)
INCLUDE_PATH=$(INCLUDE_DIR)$(PATH_SEP)
LIB_PATH=$(LIB_DIR)$(PATH_SEP)
DDOC_PATH=$(DDOC_DIR)$(PATH_SEP)

include common.make

DCFLAGS_IMPORT=-I.
DCFLAGS_LINK=
ifeq ($(OS),Posix)
	DCFLAGS_LINK=$(DLINKERFLAG)-ldl
endif

SOURCES=$(wildcard $(SOURCE_PATH)*.d)
HEADERS=$(patsubst %.d,$(INCLUDE_PATH)%.di,$(SOURCES))
OBJECTS=$(patsubst $(SOURCE_PATH)%.d,$(BUILD_PATH)%.o,$(SOURCES))
PICOBJECTS=$(patsubst $(SOURCE_PATH)%.d,$(BUILD_PATH)%.pic.o,$(SOURCES))
DDOCUMENTATIONS=$(patsubst $(SOURCE_PATH)%.d,$(DDOC_PATH)%.html,$(SOURCES))
DDOC_FLAGS=$(DDOC_PATH)candydoc$(PATH_SEP)candy.ddoc $(DDOC_PATH)candydoc$(PATH_SEP)modules.ddoc

STATIC_LIB_NAME=$(LIB_PATH)$(STATIC_LIB_PREFIX)$(PROJECT_NAME)$(STATIC_LIB_EXT)
SHARED_LIB_NAME=$(LIB_PATH)$(SHARED_LIB_PREFIX)$(PROJECT_NAME)$(SHARED_LIB_EXT)

.PHONY : clean

all: shared-lib static-lib header ddoc

shared-lib: $(SHARED_LIB_NAME)
static-lib: $(STATIC_LIB_NAME)
header: $(HEADERS)
ddoc: $(DDOCUMENTATIONS)

$(STATIC_LIB_NAME): $(OBJECTS)
	$(MKDIR) $(LIB_DIR)
	ar rcs $@ $^

$(SHARED_LIB_NAME): $(PICOBJECTS)
	$(MKDIR) $(LIB_DIR)
	$(DC) -shared $(OUTPUT)$@ $^

$(BUILD_PATH)%.pic.o: $(SOURCE_PATH)%.d
	$(DC) $(DCFLAGS) $(FPIC) $(DCFLAGS_LINK) $(DCFLAGS_IMPORT) -c $< $(OUTPUT)$@

$(BUILD_PATH)%.o: $(SOURCE_PATH)%.d
	$(DC) $(DCFLAGS) $(DCFLAGS_LINK) $(DCFLAGS_IMPORT) -c $< $(OUTPUT)$@

$(INCLUDE_PATH)%.di : %.d
	$(DC) $(DCFLAGS) $(DCFLAGS_LINK) $(DCFLAGS_IMPORT) -c $(NO_OBJ) $< $(HF)$@

$(DDOC_PATH)%.html : $(SOURCE_PATH)%.d
	$(DC) $(DCFLAGS) $(DCFLAGS_LINK) $(DCFLAGS_IMPORT) -c $(NO_OBJ) $< $(DF)$@  $(DDOC_FLAGS)

clean: clean-static-lib clean-shared-lib clean-objects clean-shared-objects clean-header clean-ddoc

clean-static-lib:
	$(DEL) $(STATIC_LIB_NAME)

clean-shared-lib:
	$(DEL) $(SHARED_LIB_NAME)

clean-objects:
	$(DEL) $(OBJECTS)

clean-shared-objects:
	$(DEL) $(PICOBJECTS)

clean-header:
	$(DEL) $(HEADERS)

clean-ddoc:
	$(DEL) $(DDOCUMENTATIONS)
