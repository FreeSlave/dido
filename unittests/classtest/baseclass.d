module unittests.classtest.baseclass;

class IDfoo
{
protected:
    int payload;
public:
    abstract int dfoo(int a, int b);
    int getPayload() const {
        return payload;
    }
    abstract void testThrow();
}