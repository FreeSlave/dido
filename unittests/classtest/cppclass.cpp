#include <string.h>
#include <exception>

#ifndef _WIN32
extern void cppRethrower();
#endif

class Cpp
{
public:
    virtual int getPayload() const
    {
        return payload;
    }
    virtual int doThing(int a, int b)
    {
        payload = a+b;
        return payload;
    }
    virtual void testThrow()
    {
#ifndef _WIN32
        throw std::exception();
#endif
    }
protected:
    int payload;
};

class Cpp2 : public Cpp
{
public:
    virtual int doThing(int a, int b)
    {
        payload = a*b;
        return payload;
    }
};

extern "C"
{
    Cpp* dido_create(const char* className)
    {
        if (strcmp(className, "Cpp") == 0)
            return new Cpp();
        if (strcmp(className, "Cpp2") == 0)
            return new Cpp2();
        return 0;
    }
    
    const char** dido_classNames()
    {
        static const char* names[] = {"Cpp", "Cpp2", 0};
#ifndef _WIN32
        std::set_terminate(cppRethrower);
#endif
        return names;
    }

    void dido_destroy(Cpp* c)
    {
        delete c;
    }
}