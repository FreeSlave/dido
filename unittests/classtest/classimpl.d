module unittests.classtest.classimpl;
import unittests.classtest.baseclass;
import dido.classexport;

class DfooPlus : IDfoo
{
public:
    override int dfoo(int a, int b) 
    {
        payload = a+b;
        return payload;
    }
    override void testThrow() {
        version(Windows){
        }
        else
            throw new Exception("From DfooPlus");
    }
}

class DfooMult : IDfoo
{
public:
    override int dfoo(int a, int b) 
    {
        payload = a*b;
        return payload;
    }
    override void testThrow() {
        version(Windows){
        }
        else
            throw new Exception("From DfooMult");
    }
}

mixin(ExportClasses!(IDfoo, DfooPlus, DfooMult));