module unittests.libtest.funcs;

version(LDC)
{
    void main(){}
}

export int dfoo(int a, int b)
{
    return a+b;
}

export extern(C) int cfoo(int a, int b)
{
    return a+b;
}

export void testThrow()
{
    throw new Exception("Just a test");
}
