DC=dmd

OUTPUT=-of
NO_OBJ=-o-
HF=-Hf
HD=-Hd
DF=-Df
DD=-Dd
DLINKERFLAG=-L
FPIC=-fPIC

ifdef SystemRoot
	OS=Windows
	EXEC_EXT=.exe
	STATIC_LIB_EXT=.lib
	SHARED_LIB_EXT=.dll
	STATIC_LIB_PREFIX=
	SHARED_LIB_PREFIX=
	DEL=del
	MKDIR=mkdir
# getSource=$(shell dir $(SOURCE_DIR) /s /b)
	CXX=dmc
	PATH_SEP=\
	SHELL=cmd.exe
else
	OS=Posix
	EXEC_EXT=.bin
	STATIC_LIB_EXT=.a
	SHARED_LIB_EXT=.so
	STATIC_LIB_PREFIX=lib
	SHARED_LIB_PREFIX=lib
	DEL=rm -f
	MKDIR=mkdir -p
# getSource=$(shell find $(SOURCE_DIR) -name "*.d")
	CXX=g++
	PATH_SEP=/
	SHELL=sh
endif

export DC
export OUTPUT
export NO_OBJ
export HF
export HD
export DF
export DD
export DLINKERFLAG
export FPIC

export OS
export EXEC_EXT
export STATIC_LIB_EXT
export SHARED_LIB_EXT
export STATIC_LIB_PREFIX
export SHARED_LIB_PREFIX
export DEL
export MKDIR
export DCFLAGS_LINK
export getSource
export CXX
export PATH_SEP
export SHELL